import pygame

pygame.init()

screen = pygame.display.set_mode((1920, 980))

f = [False, False, False, False, False, False, False, False, False]
# all dynamic block values

rx = [0, 640, 1280, 0, 960, 0, 640, 1280, 0, 960, 0, 640, 1280]
ry = [130, 130, 130, 290, 290, 450, 450, 450, 610, 610, 770, 770, 770]
movingRight = []
for i in range(0, 13):
    movingRight.append(True)
movl = [0, 640, 1280, 0, 960, 0, 640, 1280, 0, 960, 0, 640, 1280]
movr = [540, 1180, 1820, 860, 1820, 540, 1180, 1820, 860, 1820]
movr.append(540)
movr.append(1180)
movr.append(1820)
s1 = [5, 8, 3, 6, 10, 8, 4, 7, 4, 5, 3, 8, 5]
s2 = [5, 8, 3, 6, 10, 8, 4, 7, 4, 5, 3, 8, 5]

# all static block values

sx = [0, 910, 1820, 455, 1365, 0, 910, 1820, 455, 1365]
sy = [210, 210, 210, 370, 370, 530, 530, 530, 690, 690]

car2 = pygame.image.load("car_left.png")
car2 = pygame.transform.scale(car2, (100, 60))
car2 = car2.convert_alpha()

# Loading images for cars, pavement, road, obstacles, and players

car3 = pygame.image.load("car.png")
car3 = pygame.transform.scale(car3, (100, 60))
car3 = car3.convert_alpha()

pavement = pygame.image.load("pavement.jpg")
pavement = pygame.transform.scale(pavement, (1920, 40))
pavement = pavement.convert()

pavement1 = pygame.image.load("pavement.jpg")
pavement1 = pygame.transform.scale(pavement1, (1920, 100))
pavement1 = pavement1.convert()

hydrant = pygame.image.load("hydrant.png")
hydrant = pygame.transform.scale(hydrant, (100, 60))
hydrant = hydrant.convert_alpha()

road = pygame.image.load("road.jpg")
road = pygame.transform.scale(road, (1980, 120))
road = road.convert()

road1 = pygame.image.load("road.jpg")
road1 = pygame.transform.scale(road1, (1980, 160))
road1 = road1.convert()

player1_image = pygame.image.load("driver1.png")
player1_image = pygame.transform.scale(player1_image, (60, 60))
player1_image = player1_image.convert_alpha()

player2_image = pygame.image.load("driver2.png")
player2_image = pygame.transform.scale(player2_image, (60, 60))
player2_image = player2_image.convert_alpha()

# Initial positions of Player1 and Player2

x1 = 0
y1 = 0
x2 = 1920-60
y2 = 980-60

p1 = 1
p2 = 2

player1 = True
player2 = False

player1_win = False
player2_win = False

frame = pygame.time.Clock()

finished = False

scoreIncrementTimer = 0
lastFrameTicks = pygame.time.get_ticks()
time_score = 0

curr_sc_p1 = 0
curr_sc_p2 = 0

temp = 0

font = pygame.font.SysFont("comicsans", 60)

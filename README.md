# Cross the Road

Cross the Road is a multi-player, obstacle avoidance game developed in Python 3.6. It makes use of the Pygame 1.9.6 library. The player should reach the other end without colliding with any obstacles

# Instruction

The arrow keys are used to maneuver the player from one side to the other. This is applicable to both the players


# Scoring

The score of a player would depend on the obstacles he crosses and the time he takes to reach the other end.

Score obtained for crossing obstacle:

ob_score = speed_of_obstacle * 10 (for each moving obstacle) + 20 (for each static obstacle)

Final score of the player is calculated as 

final_score = ob_score + (300 - time_passed_in_multiples_of_0.1s)

# Winning

The player wins based on his final score. However, if one of the player dies, the other player wins irrespective of the score. Incase both of them die, the game is declared a draw. As a player wins, the speed of moving obstacles increases for that player, making it harder for him to cross it but also giving the player higher score for each obstacle.

# Author

Pavani Babburi

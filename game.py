import pygame
from config import *
from config1 import *

pygame.init()

while finished is False:

    player1_collide = False
    player2_collide = False

    a = 0

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            finished = True

    color = (0, 255, 0)
    color1 = (255, 255, 0)
    color2 = (150, 75, 0)
    color3 = (255, 0, 0)

    if player1 is True:

        keys1 = pygame.key.get_pressed()  # key action

        x1, y1 = key_action(keys1, x1, y1)

        for i in range(0, 13):  # moving dynamic blocks
            rx[i], movingRight[i] = mov_blocks(i, p1)

        thisFrameTicks = pygame.time.get_ticks()  # time_score
        ticksSinceLastFrame = thisFrameTicks - lastFrameTicks
        lastFrameTicks = thisFrameTicks
        scoreIncrementTimer = scoreIncrementTimer + ticksSinceLastFrame

        if scoreIncrementTimer > 100:
            time_score = time_score + 1  # Time passed
            scoreIncrementTimer = 0

        b = "Time : " + str(time_score)  # Time display
        text_time_score = font.render(b, True, (0, 0, 0))

        curr_sc_p1 = pass_score(x1, y1, curr_sc_p1, p1)  # pass score
        text_pass_score = font.render(str(curr_sc_p1), True, (0, 0, 0))

        if 300-time_score > 0:
            curr_sc = curr_sc_p1 + (300-time_score)
        else:
            curr_sc = curr_sc_p1

        for i in range(0, 13):  # collision with dynamic blocks
            player1_collide = state_col(x1, y1, rx[i], ry[i], player1_collide)

        for i in range(0, 10):  # collision with static blocks
            player1_collide = state_col(x1, y1, sx[i], sy[i], player1_collide)

        # Player1 stop condition
        if player1_collide is True or y1 >= 880:
            x1 = 0
            y1 = 0
            player1 = False
            player2 = True
            if 300-time_score > 0:
                final_player1 = curr_sc_p1 + (300-time_score)
                # Score calculation
            else:
                final_player1 = curr_sc_p1
            scoreIncrementTimer = 0
            lastFrameTicks = pygame.time.get_ticks()
            time_score = 0
            curr_sc_p1 = 0
            rectPlayer1 = pygame.Rect(x1, y1, 30, 30)
            pygame.draw.rect(screen, color1, rectPlayer1)
            temp = 1
            for i in range(0, 9):
                f[i] = False
            if player1_collide is True:
                player1_reached = False
                player1_died = True
            else:
                player1_reached = True
                player1_died = False

    if player2 is True:

        keys2 = pygame.key.get_pressed()  # key action

        x2, y2 = key_action(keys2, x2, y2)

        for i in range(0, 13):  # moving dynamic blocks
            rx[i], movingRight[i] = mov_blocks(i, p2)

        thisFrameTicks = pygame.time.get_ticks()  # time_score
        ticksSinceLastFrame = thisFrameTicks - lastFrameTicks
        lastFrameTicks = thisFrameTicks
        scoreIncrementTimer = scoreIncrementTimer + ticksSinceLastFrame

        if scoreIncrementTimer > 100:
            time_score = time_score + 1  # Time passed
            scoreIncrementTimer = 0

        b = "Time : " + str(time_score)
        text_time_score = font.render(b, True, (0, 0, 0))

        curr_sc_p2 = pass_score(x2, y2, curr_sc_p2, p2)  # pass score

        text_pass_score = font.render(str(curr_sc_p2), True, (0, 0, 0))

        if 300-time_score > 0:
            curr_sc = curr_sc_p2 + (300-time_score)
        else:
            curr_sc = curr_sc_p2

        for i in range(0, 13):  # collision with dynamic blocks
            player2_collide = state_col(x2, y2, rx[i], ry[i], player2_collide)

        for i in range(0, 10):  # collision with static blocks
            player2_collide = state_col(x2, y2, sx[i], sy[i], player2_collide)

        # Player2 stop condition
        if player2_collide is True or y2 <= 70:
            x2 = 1920-60
            y2 = 920
            player1 = False
            player2 = False
            if 300-time_score > 0:
                final_player2 = curr_sc_p2 + (300-time_score)
                # Score calculation
            else:
                final_player2 = curr_sc_p2
            curr_sc_p2 = 0
            for i in range(0, 9):
                f[i] = False
            rectPlayer2 = pygame.Rect(x2, y2, 30, 30)
            pygame.draw.rect(screen, color1, rectPlayer2)
            if player2_collide is True:
                player2_died = True
                player2_reached = False
            else:
                player2_reached = True
                player2_died = False

    if player1 is False and player2 is False:
        a = 1
        if player1_died is True:
            if player2_died is True:  # no player won  no change
                msg = font.render("Draw", True, (255, 255, 0))
                screen.blit(msg, (800, 450))
            else:  # player2 won
                msg = font.render("Player 2 won", True, (255, 255, 0))
                screen.blit(msg, (800, 450))
                player2_win = True

        elif player2_died is True:
            if player1_died is True:
                a = 1
            else:  # Player1 won
                msg = font.render("Player 1 won", True, (255, 255, 0))
                screen.blit(msg, (800, 450))

        else:
            if final_player1 > final_player2:  # Player1 won
                msg = font.render("Player 1 won", True, (255, 255, 0))
                screen.blit(msg, (800, 450))
                player1_win = True

            elif final_player2 > final_player1:  # Player2 won
                msg = font.render("Player 2 won", True, (255, 255, 0))
                screen.blit(msg, (800, 450))
                player2_win = True
            else:
                msg = font.render("Draw", True, (255, 255, 0))
                screen.blit(msg, (800, 450))

        IncrementTimer = 0
        lastFrameTicks = pygame.time.get_ticks()
        time_score = 0

        player1 = True
        player2 = False

    if player1_win is True:
        for i in range(0, 13):
            s1[i] = s1[i] + 3  # Increase speed for player1
        player1_win = False

    elif player2_win is True:
        for i in range(0, 13):
            s2[i] = s2[i] + 3  # Increase speed for player2
        player2_win = False

    screen.fill((0, 0, 255))  # Background color
    rectOne = pygame.Rect(0, 880, 1920, 100)
    rectTwo = pygame.Rect(0, 0, 1920, 100)

    rect1 = pygame.Rect(0, 220, 1920, 40)
    rect2 = pygame.Rect(0, 380, 1920, 40)
    rect3 = pygame.Rect(0, 540, 1920, 40)
    rect4 = pygame.Rect(0, 700, 1920, 40)

    rectob = []
    rectmovob = []

    for i in range(0, 10):
        rectob.append(pygame.Rect(sx[i], sy[i], 100, 60))

    for i in range(0, 13):
        rectmovob.append(pygame.Rect(rx[i], ry[i], 100, 60))

    rectPlayer1 = pygame.Rect(x1, y1, 30, 30)
    rectPlayer2 = pygame.Rect(x2, y2, 30, 30)

    screen.blit(road, (0, 100))  # Blitting road
    screen.blit(road, (0, 260))
    screen.blit(road, (0, 420))
    screen.blit(road, (0, 580))
    screen.blit(road1, (0, 740))

    screen.blit(pavement1, (0, 0))  # Blitting pavement
    screen.blit(pavement1, (0, 880))

    screen.blit(pavement, (0, 220))
    screen.blit(pavement, (0, 380))
    screen.blit(pavement, (0, 540))
    screen.blit(pavement, (0, 700))

    for i in range(0, 10):
        screen.blit(hydrant, rectob[i])  # stactic obstacles

    for i in range(0, 13):  # Dynamic obstacles
        if movingRight[i] is True:  # moving right
            screen.blit(car2, rectmovob[i])
        else:  # moving left
            screen.blit(car3, rectmovob[i])

    screen.blit(player1_image, (x1, y1))  # Blitting player1
    screen.blit(player2_image, (x2, y2))  # Blitting player2

    screen.blit(text_time_score, (50, 50))  # Blitting time

    screen.blit(text_pass_score, (500, 50))  # Blitting obstacle score

    if a == 1:
        screen.blit(msg, (800, 450))  # Blitting result
    else:
        b = "Score : " + str(curr_sc)
        text_curr_sc = font.render(b, True, (0, 0, 0))
        # Displaying the current score
        screen.blit(text_curr_sc, (1600, 50))  # Blitting current score

    pygame.display.flip()

    if a == 1:
        pygame.time.delay(1000)
        a = 0

    if temp == 1:
        pygame.time.delay(1000)
        temp = 0

    frame.tick(60)

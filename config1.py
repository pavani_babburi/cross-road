import pygame
from config import *

pygame.init()


def key_action(key, x, y):  # key concersion

        LEFT = pygame.K_LEFT
        RIGHT = pygame.K_RIGHT
        UP = pygame.K_UP
        DOWN = pygame.K_DOWN

        if key[LEFT] == 1:
            x -= 5
            if x < 0:
                x = 0
        if key[RIGHT] == 1:
            x += 5
            if x > 1920-60:
                x = 1920-60
        if key[UP] == 1:
            y -= 5
            if y < 0:
                y = 0
        if key[DOWN] == 1:
            y += 5
            if y > 980-60:
                y = 980-60
        return x, y


def state_col(x, y, left, top, previous_state):  # Collision

    state = previous_state
    if x+60 >= left and x <= left+100:
        if y >= top-60 and y <= top+60:
            state = True
    return state


def mov_blocks(i, p):  # Moving blocks
    global rx, movingRight, movl, movr
    if p == 1:
        s = s1[i]
    else:
        s = s2[i]
    if rx[i] >= movr[i]:
        movingRight[i] = False
    elif rx[i] <= movl[i]:
        movingRight[i] = True
    if movingRight[i] is True:
        rx[i] += s
    else:
        rx[i] -= s
    return rx[i], movingRight[i]

# calculate the score obtained by crossing obstacles
# Score for moving obstacles depends on their speed


def pass_score(x, y, curr_sc, p):

    global f, s1, s2
    if p == 1:
        s = s1
        if y > 220 and f[0] is False:
            for i in range(0, 3):
                if x >= movl[i] and x < movr[i]:
                    curr_sc += s[i]*10  # score = speed * 10
                    f[0] = True
                    break
        elif y > 260 and f[1] is False:
            curr_sc += 20  # direct 20 for static
            f[1] = True
        elif y > 380 and f[2] is False:
            for i in range(3, 5):
                if x >= movl[i] and x < movr[i]:
                    curr_sc += s[i]*10
                    f[2] = True
                    break
        elif y > 420 and f[3] is False:
            curr_sc += 20
            f[3] = True
        elif y > 540 and f[4] is False:
            for i in range(5, 8):
                if x >= movl[i] and x < movr[i]:
                    curr_sc += s[i]*10
                    f[4] = True
                    break
        elif y > 580 and f[5] is False:
            curr_sc += 20
            f[5] = True
        elif y > 700 and f[6] is False:
            for i in range(8, 10):
                if x >= movl[i] and x < movr[i]:
                    curr_sc += s[i]*10
                    f[6] = True
                    break
        elif y > 740 and f[7] is False:
            curr_sc += 20
            f[7] = True
        elif y > 880 and f[8] is False:
            for i in range(10, 13):
                if x >= movl[i] and x < movr[i]:
                    curr_sc += s[i]*10
                    f[8] = True
                    break
        else:
            curr_sc = curr_sc
        return curr_sc
    else:
        s = s2
        if y < 100 and f[0] is False:
            for i in range(0, 3):
                if x >= movl[i] and x < movr[i]:
                    curr_sc += s[i]*10
                    f[0] = True
                    break
        elif y < 220 and f[1] is False:
            curr_sc += 20
            f[1] = True
        elif y < 260 and f[2] is False:
            for i in range(3, 5):
                if x >= movl[i] and x < movr[i]:
                    curr_sc += s[i]*10
                    f[2] = True
                    break
        elif y < 380 and f[3] is False:
            curr_sc += 20
            f[3] = True
        elif y < 420 and f[4] is False:
            for i in range(5, 8):
                if x >= movl[i] and x < movr[i]:
                    curr_sc += s[i]*10
                    f[4] = True
                    break
        elif y < 540 and f[5] is False:
            curr_sc += 20
            f[5] = True
        elif y < 580 and f[6] is False:
            for i in range(8, 10):
                if x >= movl[i] and x < movr[i]:
                    curr_sc += s[i]*10
                    f[6] = True
                    break
        elif y < 700 and f[7] is False:
            curr_sc += 20
            f[7] = True
        elif y < 740 and f[8] is False:
            for i in range(10, 13):
                if x >= movl[i] and x < movr[i]:
                    curr_sc += s[i]*10
                    f[8] = True
                    break
        else:
            curr_sc = curr_sc
        return curr_sc
